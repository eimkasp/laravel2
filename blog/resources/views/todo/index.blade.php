@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Todo list</h1>
        <a href="{{ route('todo.index') }}">
            View all
        </a>
        <a href="{{ route('todo.indexCompleted') }}">
            View completed
        </a>
        {{-- Includinu savo todo sukurimo forma--}}
        @include('todo.components.create')

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Task title</th>
                <th>Task user</th>
                <th>

                    <a href="?order={{ $order }}">
                        Task date
                    </a>

                </th>
                <th>
                    Complete
                </th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach($todoItems as $item)
                <tr>
                    <td>
                        @if($item->status == 1)
                            <div style="text-decoration: line-through">
                                <a href="{{ route('todo.show', [$item->id]) }}">
                                    {{ $item->title }}
                                </a>

                            </div>
                        @else
                            <a href="{{ route('todo.show', [$item->id]) }}">
                                {{ $item->title }}
                            </a>
                        @endif
                    </td>
                    <td>
                        @isset( $item->user)
                            <a href="{{ route('users.show', [$item->user->id]) }}">
                                {{ $item->user->name }}
                            </a>
                        @endisset

                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td>
                        @if($item->status == 0)
                            <form method="post" action="{{ route('todo.complete', [$item->id]) }}">
                                @csrf
                                <input type="submit" value="Complete" class="btn btn-success">
                            </form>
                        @endif
                    </td>
                    <td>
                        @if($item->status == 1)
                            <form method="post" action="{{ route('todo.destroy', [$item->id]) }}">
                                @csrf
                                @method('delete')
                                <input type="submit" value="X" class="btn btn-danger">
                            </form>
                        @endif

                    </td>
                </tr>
            @endforeach
        </table>

    </div>
@endsection