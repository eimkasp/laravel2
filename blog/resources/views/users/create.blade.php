@extends('layouts.app')


@section('content')
    <div class="container">
        <h1>
            Vartotojo kurimas
        </h1>

        <form method="post" action="{{ route('users.store') }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label>Vartotojo vardas</label>
                <input required type="text" name="name" class="form-control" value="">
            </div>
            <div class="form-group">
                <label>El.pastas</label>
                <input required type="email" name="email" class="form-control" value="">
            </div>

            <div class="form-group">
                <label>El.pastas</label>
                <input required type="password" name="password" class="form-control" value="">
            </div>


            <input type="submit" class="btn btn-primary">
        </form>
    </div>

@endsection